import logo from "./logo.svg";
import "./App.css";
import { useEffect } from "react";

function App() {
  useEffect(() => {
    var myHeaders = new Headers();
    myHeaders.append(
      "Authorization",
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMzgxNTQ3N2RjYmNhMDQ2MDUyYmJiNGExNDc1YzQzYzYwZjk3NzA3OTYyN2I5YWIwNWRiMWEyNGE0ZjdhMDI0MjAwNmM3ZjExNDQzOTA0MzEiLCJpYXQiOjE2NDYyOTMzMzksIm5iZiI6MTY0NjI5MzMzOSwiZXhwIjoxODA0MDU5NzM4LCJzdWIiOiIiLCJzY29wZXMiOlsidXNlci1wdWJsaWMiXX0.X0zP464nSvucMoEG9gPC6RJzR7W3m9xEyGUR9YUOq3Z-TavoVMM0nEQZeE-TI10RjPGSC6Aw9YENe1DypPtNbwP1Dj4mKwpQ2aTZhtG665X08R-0jZq-vxMnOsg0y5UdOIbF3vchxzfqQHDu67biB_G56GFUseHWSLeT6HifARl4PNndAOQoSRHV_98IK2y6JJD69jm4Cig856XPGvMAM4qoNP2-ZTSU_3DQTTPuI94ujmb0p3jq_-L-F6oJpSQtE9yHDniGsEzShfW6jYbwJER7wax51U-3JdeK7Xr6JUz9NfVkQj1rAUuq-YhHC_7SaYAWbe6KHooqpU0XUfnrnbj2FAD3ujEW3zd5YqKh1xDUOBuUoqnvPQpnquAr4leTzDDMufmxGp_hnNLFgtjDKG0_kwX0Tj7BJGGlAb1NDtlsK7LK4PYqihaYCaaAIMcH3GDo7bnzMAoZupc5oZsluqVNfOqJS6ZzftkHtOMJZTKsEKWnDs-1PA0mL84T-_BdLdDuMyPuQGLseK2vZAj8DXGJwupVn1H3J6sZckc6JHWP7VwCWAWH2Fc4yclPr4MFhQhanbV2nhgJ4YGJ79s5dIHbUTtgTxTfkd8niX3qn9yjSpxPQU76jMpopN_874T9J064QIXku8MPrZi544NYrOg6K-TAVebb7UTpbqoNr4w"
    );

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch(
      "https://kuat1app.azurewebsites.net/api/v1/partner_registration/",
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
